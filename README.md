## Up and running

Install Ruby:

```shell
asdf plugin-add ruby
asdf install ruby 2.6.3
asdf global ruby 2.6.3
```

`cd` into this directory, install dependencies, create database:

```shell
cd amagumo
bundle
rails db:create db:migrate
```

Run development server:

```shell
rails server
```
