Role Name
=========

This role manages user accounts for applications.

Requirements
------------

This role adds and removes users and configures access to accounts via ssh. 

Our general approach is to manage users with this role, and have a separate
role which installs and configures the app. 

This role allows us centrally manage admin and developer accounts across
all the servers we manage. 

We keep user public keys checked into git under the Ansible role. So when we
create a machine, we automatically add the admin team users to the server, and
we can revoke user access to all the servers at once if someone leaves.

The initial setup of the app on the server is done by the system admin or a
project admin, someone with sudo, giving them rights to install software and
create the app directory structure. After that it can be deployed by the deploy
user, perhaps from a CI server. 


We have the following classes of accounts:

* Global system admin users / ops team

  These users have their own logins on the server. We add them to the "wheel" 
  or "admin" group, then configure sudo to allow these users to run sudo
  without a password. 

  When we provision a server, we automatically create accounts for the
  sysadmins, irrespective of the project.

* Project admins / power users

  These users have the same rights as global sysadmins, but are set up on
  per-project / server basis (controlled with inventory host/group vars).

* Deploy account

  The deploy user account is used to deploy the application to the server. 

  It has write permissions to the deploy and config directories.

* App account

  The application runs under this user account. 

  This account has write access to the directories it needs at runtime, e.g.
  logs, and read-only access to its code and config files.

* Developer users

  In dev and integration environments, we may deploy the app under the app user
  account. This allows developers to update the code and change the config.

  We add the ssh keys for developers to the account, allowing them to log in via ssh. 

* Project users

  These users are like project admins, but don't have sudo. An example might
  be an account for a customer to be able to log in and run queries against
  the db, but they don't need admin rights. 

The app and deploy accounts generally do not have sudo permissions, though we
may make a special rule to allow them to run commands to restart the app
(handled by the role that installs and configures the app, not this role). 

We generally use Upstart or Systemd to supervise the daemon, so we make a
/etc/sudoers.d config for the app which allows it to run commands like 
"service foo restart". 

For some apps, we use a "flag file" which the app daemon checks periodically. 
If the flag exists, it shuts itself down, and the supervisor restarts it. 
This eliminates the need for sudo in the app account.
