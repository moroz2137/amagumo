#!/usr/bin/env ruby
# frozen_string_literals: true

require 'fileutils'

IP_REGEX = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/

current_dir = Dir.pwd

puts "Fetching host configuration from Terraform data..."
Dir.chdir("../terraform")
ips = `terraform output web_addresses`
ips = ips.split("\n").filter { |line| line.is_a?(String) && line.match(IP_REGEX) }.map { |ip| ip.gsub(/[^0-9\.]/, '') }
key_path = File.read("terraform.tfvars").split("\n").find { |line| line.start_with?("key_path") }.split(" = ").last.gsub('"', '')
Dir.chdir(current_dir)
FileUtils.mkdir_p(File.join(current_dir, "inventory"))
output = File.open("inventory/hosts", "w")

puts "Writing host data to inventory/hosts..."

output.puts "[web_servers]"
ips.length.times do |i|
  output.printf("web-%03d\n", i + 1)
end
output.close

output = File.open("ssh.config", "w")
puts "Writing hosts to ssh.config..."
ips.each_with_index do |ip, i|
  output.printf("Host web-%03d\n", i + 1)
  output.puts "  Hostname #{ip}"
  output.puts "  IdentityFile #{key_path}"
  output.puts "  StrictHostKeyChecking no"
  output.puts "  UserKnownHostsFile /dev/null"
  output.puts
end
