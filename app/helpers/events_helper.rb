# frozen_string_literal: true

module EventsHelper
  def format_date(datetime)
    return nil unless datetime.respond_to?(:strftime)

    datetime.localtime.strftime("%Y-%m-%d %H:%M")
  end
end
