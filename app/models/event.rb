# frozen_string_literal: true

class Event < ApplicationRecord
  validates_presence_of :name, :venue, :description, :start_time, :end_time
  validates :description, length: { minimum: 10, maximum: 1000 }
  validate :end_time_gt_start_time

  mount_uploader :picture, EventPictureUploader

  before_validation :make_start_end_times

  default_scope { order(start_time: :desc) }

  scope :upcoming, -> { where("end_time > ?", Time.now()) }
  scope :past, -> { where("end_time <= ?", Time.now()) }

  attr_accessor :start_date, :end_date

  private

  def make_start_end_times
    if start_date && start_time
      str = "#{start_date}T" + start_time.strftime('%H:%M')
      self.start_time = Time.parse(str)
    end
    if end_date && end_time
      str = "#{end_date}T" + end_time.strftime('%H:%M')
      self.end_time = Time.parse(str)
    end
  end

  def end_time_gt_start_time
    if start_time && end_time && start_time >= end_time
      errors.add(:end_time, "should be later than start time")
    end
  end
end
