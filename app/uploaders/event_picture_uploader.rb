class EventPictureUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :fog

  process :auto_orient
  process resize_to_limit: [800, 800]

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url(*args)
    ActionController::Base.helpers.asset_path("placeholder.jpg")
  end

  def auto_orient
    manipulate! do |img|
      img.tap(&:auto_orient)
    end
  end

  version :thumb do
    resize_to_fit(180, 180)
  end

  version :thumb_2x do
    resize_to_fit(360, 360)
  end

  def extension_whitelist
    %w(jpg jpeg gif png)
  end
end
