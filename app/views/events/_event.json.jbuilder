# frozen_string_literal: true

json.extract! event, :id, :name, :venue, :start_time, :end_time, :description, :created_at, :updated_at
json.url event_url(event, format: :json)
