# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'events#index'
  resources :events, except: :index
  get '/past', to: 'events#past'
end
